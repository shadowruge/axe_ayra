# axe_ayra


# Sobre o Axe Ayra

O Barracão de Candomblé **Axe Ayra** é um espaço sagrado e acolhedor que celebra e preserva as tradições ancestrais do candomblé.

Situado no coração da cultura afro-brasileira, nosso barracão é um local onde a espiritualidade, a música, a dança e os rituais se unem para honrar os orixás e promover a conexão com o divino.

Aqui, a energia dos tambores ecoa em harmonia com os cantos e as danças, proporcionando um ambiente de respeito, amor e devoção. Nossa casa é um espaço de aprendizado, onde os ensinamentos ancestrais são transmitidos e compartilhados, guiando aqueles que buscam compreender e se conectar com as raízes do candomblé.

Compreendemos a importância da preservação das tradições e valores do candomblé, promovendo a educação espiritual, o respeito à diversidade e a valorização da cultura afro-brasileira. No Barracão de Candomblé **Axe Ayra**, todos são bem-vindos para aprender, celebrar e fortalecer os laços com a espiritualidade, em um ambiente de acolhimento e respeito mútuo.<br>
[Link de acesso ao site](https://shadowruge.github.io/axe_ayra/)
